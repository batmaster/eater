package com.batmasterio.eater.story;

public class StoryCodeModel extends StoryModel {

    private String codeText;
    private String dueText;

    public StoryCodeModel(String logoImageUrl, String bodyText, String codeText) {
        this(logoImageUrl, bodyText, codeText, "");
    }

    public StoryCodeModel(String logoImageUrl, String bodyText, String codeText, String dueText) {
        super(logoImageUrl, bodyText, StoryModel.TYPE_CODE);
        this.codeText = codeText;
        this.dueText = dueText;
    }

    public String getCodeText() {
        return codeText;
    }

    public void setCodeText(String codeText) {
        this.codeText = codeText;
    }

    public String getDueText() {
        return dueText;
    }

    public void setDueText(String dueText) {
        this.dueText = dueText;
    }
}
