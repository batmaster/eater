package com.batmasterio.eater.story;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.batmasterio.eater.R;

public class StoryDetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_story_detail);
    }
}
