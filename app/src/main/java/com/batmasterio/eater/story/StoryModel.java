package com.batmasterio.eater.story;

class StoryModel {

    private String logoImageUrl;
    private String bodyText;

    private int type;

    public static final int TYPE_DEFAULT = 0;
    public static final int TYPE_IMAGE = 1;
    public static final int TYPE_CODE = 2;
    public static final int TYPE_VIDEO = 3;
    public static final int TYPE_LOCATION = 4;

    protected StoryModel(String logoImageUrl, String bodyText, int type) {
        this.logoImageUrl = logoImageUrl;
        this.bodyText = bodyText;
        this.type = type;
    }

    public String getLogoImageUrl() {
        return logoImageUrl;
    }

    public void setLogoImageUrl(String logoImageUrl) {
        this.logoImageUrl = logoImageUrl;
    }

    public String getBodyText() {
        return bodyText;
    }

    public void setBodyText(String bodyText) {
        this.bodyText = bodyText;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
