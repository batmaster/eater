package com.batmasterio.eater.story;

import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.VideoView;

import com.batmasterio.eater.R;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;

class StoryListViewAdapter extends RecyclerView.Adapter<StoryListViewAdapter.ViewHolderStory> {

    private Context context;
    private List<StoryModel> storyModelList;

    public StoryListViewAdapter(Context context, List<StoryModel> storyModelList) {
        this.context = context;
        this.storyModelList = storyModelList;
    }

    @Override
    public int getItemViewType(int position) {
        return storyModelList.get(position).getType();
    }

    @Override
    public int getItemCount() {
        return storyModelList.size();
    }

    @Override
    public StoryListViewAdapter.ViewHolderStory onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = null;
        ViewHolderStory vh = null;
        switch (viewType) {
            case StoryModel.TYPE_DEFAULT:
            case StoryModel.TYPE_CODE:
                v = (View) LayoutInflater.from(parent.getContext()).inflate(R.layout.story_code_card, parent, false);
                vh = new StoryListViewAdapter.ViewHolderStoryCode(v);
                break;
            case StoryModel.TYPE_IMAGE:
                v = (View) LayoutInflater.from(parent.getContext()).inflate(R.layout.story_image_card, parent, false);
                vh = new ViewHolderStoryImage(v);
                break;
            case StoryModel.TYPE_VIDEO:
                v = (View) LayoutInflater.from(parent.getContext()).inflate(R.layout.story_video_card, parent, false);
                vh = new ViewHolderStoryVideo(v);
                break;
            case StoryModel.TYPE_LOCATION:
                v = (View) LayoutInflater.from(parent.getContext()).inflate(R.layout.story_location_card, parent, false);
                vh = new ViewHolderStoryLocation(v);
                break;
        }

        return vh;
    }

    @Override
    public void onBindViewHolder(final StoryListViewAdapter.ViewHolderStory holder, final int position) {
        if (holder.getItemViewType() == StoryModel.TYPE_DEFAULT || holder.getItemViewType() == StoryModel.TYPE_CODE) {
            ViewHolderStoryCode h = (ViewHolderStoryCode) holder;
            StoryCodeModel m = (StoryCodeModel) storyModelList.get(position);

            h.textViewDue.setText(m.getDueText());
            h.textViewCode.setText(m.getCodeText());
            h.textViewBody.setText(m.getBodyText());
            Glide.with(context)
                    .load(m.getLogoImageUrl())
                    .apply(new RequestOptions().fitCenter())
                    .into(h.imageViewLogo);

        }
        else if (holder.getItemViewType() == StoryModel.TYPE_IMAGE) {
            ViewHolderStoryImage h = (ViewHolderStoryImage) holder;
            StoryImageModel m = (StoryImageModel) storyModelList.get(position);

            h.imageViewMenu.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    context.startActivity(new Intent(context, StoryDetailActivity.class));
                }
            });
            h.textViewBody.setText(m.getBodyText());
            Glide.with(context)
                    .load(m.getLogoImageUrl())
                    .apply(new RequestOptions().fitCenter())
                    .into(h.imageViewLogo);
            Glide.with(context)
                    .load(m.getTitleImageUrl())
                    .apply(new RequestOptions().centerCrop())
                    .into(h.imageViewTitle);
        }
        else if (holder.getItemViewType() == StoryModel.TYPE_VIDEO) {
            final ViewHolderStoryVideo h = (ViewHolderStoryVideo) holder;
            StoryVideoModel m = (StoryVideoModel) storyModelList.get(position);

            h.textViewBody.setText(m.getBodyText());
            Glide.with(context)
                    .load(m.getLogoImageUrl())
                    .apply(new RequestOptions().fitCenter())
                    .into(h.imageViewLogo);

            h.videoViewTitle.setVideoPath(m.getTitleVideoUrl());
            MediaController mediaController = new MediaController(h.videoViewTitle.getContext());
            mediaController.setAnchorView(h.videoViewTitle);
            mediaController.hide();
            h.videoViewTitle.setMediaController(mediaController);

            h.videoViewTitle.refreshDrawableState();
            h.videoViewTitle.setDrawingCacheEnabled(true);
            h.videoViewTitle.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_AUTO);

            h.videoViewTitle.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
//                    mp.setVolume(0f, 0f);
                    mp.setLooping(true);
                    h.videoViewTitle.start();
                }
            });
        }
        else if (holder.getItemViewType() == StoryModel.TYPE_LOCATION) {
            ViewHolderStoryLocation h = (ViewHolderStoryLocation) holder;
            StoryLocationModel m = (StoryLocationModel) storyModelList.get(position);

            h.textViewBody.setText(m.getBodyText());
            Glide.with(context)
                    .load(m.getLogoImageUrl())
                    .apply(new RequestOptions().fitCenter())
                    .into(h.imageViewLogo);
            h.setMap(m.getLat(), m.getLng(), m.getZoom(), m.getLocationTitle());
        }
    }

    class ViewHolderStory extends RecyclerView.ViewHolder {

        private View view;
        private ImageView imageViewLogo;
        private TextView textViewBody;

        public ViewHolderStory(View v) {
            super(v);
            view = v;

            imageViewLogo = (ImageView) v.findViewById(R.id.imageViewLogo);
            textViewBody = (TextView) v.findViewById(R.id.textViewBody);
        }
    }

    class ViewHolderStoryImage extends ViewHolderStory {

        private View view;
        private ImageView imageViewMenu;
        private ImageView imageViewTitle;
        private ImageView imageViewLogo;
        private TextView textViewBody;


        public ViewHolderStoryImage(View v) {
            super(v);
            view = v;

            imageViewMenu = (ImageView) v.findViewById(R.id.imageViewMenu);
            imageViewTitle = (ImageView) v.findViewById(R.id.imageViewTitle);
            imageViewLogo = (ImageView) v.findViewById(R.id.imageViewLogo);
            textViewBody = (TextView) v.findViewById(R.id.textViewBody);
        }
    }

    class ViewHolderStoryVideo extends ViewHolderStory {

        private View view;
        private VideoView videoViewTitle;
        private ImageView imageViewLogo;
        private TextView textViewBody;


        public ViewHolderStoryVideo(View v) {
            super(v);
            view = v;

            videoViewTitle = (VideoView) v.findViewById(R.id.videoViewTitle);
            imageViewLogo = (ImageView) v.findViewById(R.id.imageViewLogo);
            textViewBody = (TextView) v.findViewById(R.id.textViewBody);
        }
    }

    class ViewHolderStoryCode extends ViewHolderStory {

        private View view;
        private TextView textViewDue;
        private TextView textViewCode;
        private ImageView imageViewLogo;
        private TextView textViewBody;

        public ViewHolderStoryCode(View v) {
            super(v);
            view = v;

            textViewDue = (TextView) v.findViewById(R.id.textViewDue);
            textViewCode = (TextView) v.findViewById(R.id.textViewCode);
            imageViewLogo = (ImageView) v.findViewById(R.id.imageViewLogo);
            textViewBody = (TextView) v.findViewById(R.id.textViewBody);
        }
    }

    class ViewHolderStoryLocation extends ViewHolderStory {

        private View view;
        private MapView mapView;
        private ImageView imageViewLogo;
        private TextView textViewBody;

        public ViewHolderStoryLocation(View v) {
            super(v);
            view = v;

            imageViewLogo = (ImageView) v.findViewById(R.id.imageViewLogo);
            textViewBody = (TextView) v.findViewById(R.id.textViewBody);

            mapView = (MapView) v.findViewById(R.id.mapView);
            mapView.onCreate(null);
        }

        public void setMap(final double lat, final double lng, final float zoom, final String locationTitle) {
            mapView.getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(GoogleMap googleMap) {
                    googleMap.setTrafficEnabled(true);

                    LatLng l = new LatLng(lat, lng);
                    googleMap.addMarker(new MarkerOptions().position(l).title(locationTitle).snippet(locationTitle + "3a2sd31asd")).showInfoWindow();
                    googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(l, zoom));
                }
            });
            mapView.onResume();
        }
    }
}
