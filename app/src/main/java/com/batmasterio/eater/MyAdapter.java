package com.batmasterio.eater;

import android.net.wifi.ScanResult;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by batmaster on 7/5/2018 AD.
 */

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {

    private List<ScanResult> wifiScanList;

    public MyAdapter() {
        this.wifiScanList = new ArrayList<ScanResult>();
    }

    public MyAdapter(List<ScanResult> wifiScanList) {
        this.wifiScanList = wifiScanList;
    }

    @Override
    public MyAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = (View) LayoutInflater.from(parent.getContext()).inflate(R.layout.listview_wifi_row, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.textviewSSID.setText(wifiScanList.get(position).SSID);
        holder.textviewBSSID.setText(wifiScanList.get(position).BSSID);
        String level = "";
        int l = wifiScanList.get(position).level;
        if (l > -50) {
            level = "แรงมาก";
        }
        else if (l > -60 ){
            level = "แรง";
        }
        else if (l > -70 ){
            level = "ดี";
        }
        else {
            level = "อ่อน";
        }
        holder.textviewLevel.setText(level);

    }

    @Override
    public int getItemCount() {
        return wifiScanList.size();
    }

    public void setWifiScanList(List<ScanResult> wifiScanList) {
        this.wifiScanList = wifiScanList;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public View view;
        public TextView textviewSSID;
        public TextView textviewBSSID;
        public TextView textviewLevel;

        public ViewHolder(View v) {
            super(v);
            view = v;

            textviewSSID = (TextView) v.findViewById(R.id.textviewSSID);
            textviewBSSID = (TextView) v.findViewById(R.id.textviewBSSID);
            textviewLevel = (TextView) v.findViewById(R.id.textviewLevel);
        }
    }
}
