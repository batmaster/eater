package com.batmasterio.eater.story;

public class StoryImageModel extends StoryModel {

    private String titleImageUrl;

    public StoryImageModel(String logoImageUrl, String bodyText, String titleImageUrl) {
        super(logoImageUrl, bodyText, StoryModel.TYPE_IMAGE);
        this.titleImageUrl = titleImageUrl;
    }

    public String getTitleImageUrl() {
        return titleImageUrl;
    }

    public void setTitleImageUrl(String titleImageUrl) {
        this.titleImageUrl = titleImageUrl;
    }
}
