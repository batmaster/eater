package com.batmasterio.eater.story;

public class StoryVideoModel extends StoryModel {

    private String titleVideoUrl;

    public StoryVideoModel(String logoImageUrl, String bodyText, String titleVideoUrl) {
        super(logoImageUrl, bodyText, StoryModel.TYPE_VIDEO);
        this.titleVideoUrl = titleVideoUrl;
    }

    public String getTitleVideoUrl() {
        return titleVideoUrl;
    }

    public void setTitleVideoUrl(String titleVideoUrl) {
        this.titleVideoUrl = titleVideoUrl;
    }
}
