package com.batmasterio.eater.story;

public class StoryLocationModel extends StoryModel {

    private double lat;
    private double lng;
    private float zoom;
    private String locationTitle;

    public StoryLocationModel(String logoImageUrl, String bodyText, double lat, double lng, float zoom, String locationTitle) {
        super(logoImageUrl, bodyText, StoryModel.TYPE_LOCATION);
        this.lat = lat;
        this.lng = lng;
        this.zoom = zoom;
        this.locationTitle = locationTitle;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public float getZoom() {
        return zoom;
    }

    public void setZoom(float zoom) {
        this.zoom = zoom;
    }

    public String getLocationTitle() {
        return locationTitle;
    }

    public void setLocationTitle(String locationTitle) {
        this.locationTitle = locationTitle;
    }
}
